# Get Started

### Welcome to Zilla!

The Get Started guides below will walk you through the steps to build and secure a Todo application using Apache Kafka with the Zilla engine.

### Build the Todo Application

Our [Build the Todo Application](build-todo-app.md) guide shows you how to create your first application with Zilla, using Apache Kafka with Kafka Streams to implement the Tasks service that maintains a shared Todo List of tasks.

### Secure the Todo Application

Our [Secure the Todo Application](secure-todo-app.md) guide shows you how to secure the Tasks API using JWT access tokens to allow read and write access only to authorized clients.

### Connect your Kafka

Our [Connect your Kafka](../connect-your-kafka/) guide shows you how to change the Zilla configuration to use your own Kafka cluster instead of using a local Kafka deployment in a docker stack.
