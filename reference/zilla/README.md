---
description: The command line interface to control the Zilla runtime.
---

# Zilla Runtime CLI (zilla)

The Zilla Runtime command line interface uses the [Zilla Runtime Configuration](../zilla.yaml/) to control and observe the Zilla runtime.

### Commands

The Zilla Runtime command line interface supports the following commands.

|                            |                          |
| -------------------------- | ------------------------ |
| [`zilla help`](clean.md)   | Display help information |
| [`zilla load`](load.md) 🚧 | Show engine load         |
| [`zilla start`](start.md)  | Start engine             |
| [`zilla stop`](stop.md)    | Stop engine              |
| [`zilla tune`](tune.md)    | Tune engine              |
