---
description: Shows help information for commands
---

# zilla help

### Description

The `zilla help` command shows help information about available commands, or more information for a specific command.

### Usage

```
zilla help [command]
```

### Examples

```
./zilla help start
```
