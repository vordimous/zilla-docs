---
description: Stops the engine
---

# zilla stop

The `zilla stop` command signals the runtime engine to stop.

### Usage

```bash
zilla stop
```

### Examples

```bash
./zilla start
started
...
```

```bash
./zilla stop
```

```bash
...
stopped
```

See [Zilla examples](https://github.com/aklivity/zilla/tree/develop/examples) on GitHub.
