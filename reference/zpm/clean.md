---
description: Shows help information for commands
---

# zpm help

### Description

The `zpm help` command shows help information about available commands, or more information for a specific command.

### Usage

```
zpm help [command]
```

### Examples

```
./zpmw help install
```
